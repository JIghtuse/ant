#!/usr/bin/env python3

import os
import sys


virtualenv = os.path.join(
    os.environ['OPENSHIFT_REPO_DIR'],
    'virtenv',
    'bin',
    'activate_this.py')

sys.path.append(virtualenv)

try:
    exec_namespace = dict(__file__=virtualenv)
    with open(virtualenv, 'rb') as activate_file:
        code = compile(activate_file.read(), virtualenv, 'exec')
        exec(code, exec_namespace)
except IOError:
    pass

from main import app as application
