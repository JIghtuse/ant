import os
from flask import Flask, render_template, request, redirect, url_for
import pymongo

app = Flask(__name__)


def mongo_get_ants():
    client = pymongo.MongoClient(os.environ['OPENSHIFT_MONGODB_DB_URL'])
    db = client.ant_db
    return db.ants


@app.route('/')
def homepage():
    ants = mongo_get_ants().find()
    return render_template("homepage.html", ants=ants)


def valid_ant(s):
    input_error = None
    if not s:
        input_error = "Нет анта!"
    if not input_error and not s.endswith("ант"):
        input_error = "Это не ант!"

    ant_exists = mongo_get_ants().find_one({"value": s})

    if not input_error and ant_exists:
        input_error = "Такой ант уже есть!"

    if input_error is None:
        return True, None
    else:
        return False, input_error


@app.route('/new', methods=["GET", "POST"])
def new_ant():
    if request.method == "GET":
        return render_template("new_ant.html")
    else:
        ant = request.form.get("ant-word")
        ant_is_valid, error = valid_ant(ant)
        if ant_is_valid:
            mongo_get_ants().insert({"value": ant, "author": "someone"})
            return redirect(url_for("homepage"))
        else:
            return render_template("new_ant.html", ant=ant, input_error=error)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
